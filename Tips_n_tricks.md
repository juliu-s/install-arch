[[_TOC_]]

# SSDs and other flash-memory based storage devices 

* See [this wiki](https://wiki.archlinux.org/title/Solid_state_drive) for complete documentation
* Periodic TRIM

## Install package

```shell-session
pacman -S util-linux
```

## Start and enable timer

```shell-session
systemctl enable fstrim.timer
systemctl start fstrim.timer
```

## Systemd timers

```shell-session
sudo systemctl list-timers
```

# Switch to pipewire

* See [this wiki](https://wiki.archlinux.org/title/PipeWire) for complete documentation

## Install packages

```shell-session
pacman -S pipewire pipewire-alsa pipewire-jack pipewire-pulse
```

## Check result

```shell-session
pactl info
```

# Integrate Openconnect (VPN) into NetworkManager

* See [this wiki](https://wiki.archlinux.org/title/OpenConnect) for complete documentation

## Install package

```shell-session
pacman -S openconnect networkmanager-openconnect
```

## Restart service

```shell-session
systemctl restart NetworkManager.service
```

# Bluetooth

* See [this wiki](https://wiki.archlinux.org/title/bluetooth) for complete documentation

## Install packages

```shell-session
pacman -S bluez gnome-bluetooth-3.0
```

## Update config

* Sometimes pairing with devices fails

```shell-session
sudo nvim /etc/bluetooth/main.config
```

* Uncomment and change the following parameter: `FastConnectable = true`

## Start and enable service

```shell-session
systemctl enable bluetooth.service
systemctl start bluetooth.service
```

## Audio

* Sometimes the audio devices doesn't show up

```shell-session
systemctl --user restart pipewire
```

## Logs

* Shows what's going on

```shell-session
sudo journalctl -u bluetooth.service -f
journalctl --user -u pipewire.service -f
```

# The week starts on monday

* When using the `en_US` locale, change `first_weekday 1` to `first_weekday 2` or insert it at `LC_TIME`

```shell-session
sudo sed -i '/END\ LC_TIME/ifirst_weekday\ 2' /usr/share/i18n/locales/en_US
sudo locale-gen
```

* Logout and login again

# Scaling

* See [this wiki](https://wiki.archlinux.org/title/HiDPI) for complete documentation

## Text

* For example:

```shell-session
gsettings set org.gnome.desktop.interface text-scaling-factor 1.25
```

## Everything

For scaling between 100% and 200%

```shell-session
gsettings reset org.gnome.mutter experimental-features "['scale-monitor-framebuffer']"
```
* Logout and login again
* Go to settings => Display => Scale

### Reset

```shell-session
gsettings reset org.gnome.mutter experimental-features
```

* Logout and login again

# Prevent going to sleep when closing lid

```shell-session
sudo nvim /etc/systemd/logind.conf
```

```text
[Login]
HandleLidSwitch=ignore
HandleLidSwitchExternalPower=ignore
HandleLidSwitchDocked=ignore
```

```shell-session
sudo systemctl restart systemd-logind.service
```

# Prevent WiFi going to sleep

```shell-session
sudo pacman -S iw
iw dev wlan0 get power_save
sudo iw dev wlan0 set power_save off
```

# SSH-agent

[Since GNOME Keyring doesn't act as a wrapper around `ssh-agent` anymore](https://wiki.archlinux.org/title/GNOME/Keyring#SSH_keys)

```shell-session
sudo pacman -S gcr-4
systemctl --user gcr-ssh-agent.socket
# Put this in your .bashrc or .zshrc
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/gcr/ssh"
```

# Removing unused packages

```shell-session
# List unused packages
sudo pacman -Qtdq
# Remove unused packages
sudo pacman -R $(pacman -Qtdq)
```

# Clear pkg cache

```shell-session
# Remove all pkg except those installed
sudo pacman -Sc
# Remove all files
sudo pacman -Scc
```

[[_TOC_]]

# Intro

This page describes installing Arch Linux with several options but no GUI.
For a GUI see [Gnome.md](Gnome.md). 
For other tips and tricks see [this page](Tips_n_tricks.md).

* ARM (aarch64) / x86_64
* EFI / BIOS
* GDISK / FDISk
* LUKS and LVM / No LUKS and LVM

# Prepare for install

* Download the Arch ISO [from here for x86_64](https://www.archlinux.org/download/) or [here for ARM aarch64](https://archboot.com/iso/aarch64/latest/)
* Use dd or [BalenaEtcher](https://www.balena.io/etcher/)

```shell-session
dd bs=4M if=/path/to/archlinux.iso of=/dev/sdx conv=fsync oflag=direct status=progress
```

# Set keyboard layout

```shell-session
loadkeys us
```

# Enable NTP

```shell-session
timedatectl set-ntp true
```

# Connect to WiFi

* [Complete documentation](https://wiki.archlinux.org/title/Iwd#iwctl)

```shell-session
iwctl 
[iwd]# device list
[iwd]# station *device* scan
[iwd]# station *device* get-networks
[iwd]# station *device* connect *SSID*
[iwd]# CTRL + d
```

# Test your connection

```shell-session
ping -c 3 www.nsa.gov
```

# Prepare disk

## EFI with LVM and LUKS

Create 2 partitions, one for boot partition and one for LUKS encrypted partition. Adjust the sizes to your requirements

```shell-session
fdisk -l
gdisk /dev/sda
```

```text
GPT fdisk (gdisk) version 1.0.1

Partition table scan:
  MBR: protective
  BSD: not present
  APM: not present
  GPT: present

Found valid GPT with protective MBR; using GPT.

Command (? for help): o
This option deletes all partitions and creates a new protective MBR.
Proceed? (Y/N): Y

Command (? for help): n
Partition number (1-128, default 1):
First sector (34-62914560, default = 2048) or {+-}size{KMGTP}:
Last sector (2048-62914560, default = 62914560) or {+-}size{KMGTP}: +1024M
Current type is 'Linux filesystem'
Hex code or GUID (L to show codes, Enter = 8300): EF00
Changed type of partition to 'EFI System'

Command (? for help): n
Partition number (2-128, default 2):
First sector (34-62914560, default = 2099200) or {+-}size{KMGTP}:
Last sector (2099200-62914560, default = 62914560) or {+-}size{KMGTP}:
Current type is 'Linux filesystem'
Hex code or GUID (L to show codes, Enter = 8300):
Changed type of partition to 'Linux filesystem'

Command (? for help): p
Disk /dev/sda: 62914560 sectors, 30.0 GiB
Model: VBOX HARDDISK
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): 631F31D8-19CA-4BF3-9E33-6A2E30555438
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 62914526
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048         2099199   1024.0 MiB  EF00  EFI System
   2         2099200        62914526   29.0 GiB    8300  Linux filesystem

Command (? for help): w
```

### Create filesystems, encrypt, create LVM and mount

Create boot, root and a separate home partition inside encrypted LVM on /dev/sda2. Adjust the sizes to your requirements

```shell-session
mkfs.vfat -F32 /dev/sda1

cryptsetup -v luksFormat /dev/sda2
cryptsetup luksOpen /dev/sda2 luks

pvcreate /dev/mapper/luks
vgcreate vg0 /dev/mapper/luks

lvcreate -L 4G vg0 -n swap
lvcreate -L 10G vg0 -n root
lvcreate -l +100%FREE vg0 -n home

mkswap /dev/mapper/vg0-swap
mkfs.ext4 /dev/mapper/vg0-root
mkfs.ext4 /dev/mapper/vg0-home

mount /dev/mapper/vg0-root /mnt
mkdir -p /mnt/home
mkdir -p /mnt/boot
mount /dev/mapper/vg0-home /mnt/home
mount /dev/sda1 /mnt/boot
swapon /dev/mapper/vg0-swap
```

## BIOS (without LVM and LUKS)

### Create filesystems and mount

* Create 2 partitions on a 8GB disk, one root partition including boot and one for swap. Adjust the sizes to your requirements

```shell-session
fdisk -l
fdisk /dev/sda
n
p
1
<enter>
+6G
a

n
p
2
<enter>
<enter>
t
2
82
p
w
fdisk -l
```

```shell-session
mkfs.ext4 /dev/sda1
mkswap /dev/sda2
mount /dev/sda1 /mnt
swapon /dev/sda2
```

# Install packages 

**NOTE:** Add the `iwd` and `wpa_supplicant` packages if you want to manage wireless connections from the cli

## EFI

```shell-session
pacstrap /mnt base base-devel linux linux-firmware lvm2 neovim zsh efibootmgr man-db
```

## BIOS

```shell-session
pacstrap /mnt base base-devel linux linux-firmware neovim zsh grub man-db
```

# Generate fstab

```shell-session
genfstab -pU /mnt > /mnt/etc/fstab
cat /mnt/etc/fstab
```

# Chroot into new system

```shell-session
arch-chroot /mnt
```

# Set timezone

```shell-session
ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
```

# Update hardwareclock

```shell-session
hwclock --systohc
```

# Set hostname

```shell-session
echo <your-hostname> > /etc/hostname
```

# Update /etc/hosts

```shell-session
nvim /etc/hosts
```

```text
127.0.0.1   localhost
::1         localhost
127.0.0.1   <your-hostname>.localdomain
::1         <your-hostname>.localdomain
```

# Enable systemd-networkd and resolved service

```shell-session
systemctl enable systemd-networkd.service
systemctl enable systemd-resolved.service
```

## Optional configure static IP

Create a `.network` file to configure your network interface. Create a file with the name of the device name.
You can also do this after the first boot.

```shell-session
networkctl list
..
nvim /etc/systemd/network/eno1.network
```

```text
[Match]
Name=eno1

[Network]
Address=192.168.1.2/24
Gateway=192.168.1.1
DNS=192.168.1.1
```

## Optional configure DHCP (with autoconnect to WiFi)

```shell-session
networkctl list
..
nvim /etc/systemd/network/wlan0.network
```

```text
[Match]
Name=wlan0

[Network]
DHCP=yes
```

```shell-session
nvim /etc/wpa_supplicant/wpa_supplicant-<interface>.conf
..
nvim /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
```

```text
ctrl_interface=/run/wpa_supplicant
update_config=1

network={
    ssid="foowifi"
    psk="foopassword"
}
```

```shell-session
systemctl enable wpa_supplicant@<interface>.service
..
systemctl enable wpa_supplicant@wlan0.service
systemctl start wpa_supplicant@wlan0.service
```

# Add user and update the password, also for root

```shell-session
passwd
useradd -m -G wheel -s /bin/zsh <username>
passwd <username>
```

# Update the sudoers file

* Allow your user to do sudo

```shell-session
visudo
```

```text
## Uncomment to allow members of group wheel to execute any command
%wheel ALL=(ALL) ALL
```

# Set locales

* Uncomment `en_US.UTF-8` in /etc/locale.gen

```shell-session
nvim /etc/locale.gen
```

# Generate locale

```shell-session
echo "LANG=en_US.UTF-8" > /etc/locale.conf
locale-gen
```

# Install bootmanager

## Systemd-boot

```shell-session
bootctl --path=/boot install
```

## GRUB

* Install GRUB

```shell-session
grub-install /dev/sda
```

* Generate GRUB config

```shell-session
grub-mkconfig -o /boot/grub/grub.cfg
```

# Update /etc/mkinitcpio.conf

* This is needed for LVM and LUKS, for BIOS the defaults are sufficient

```text
MODULES=(ext4)
.
.
.
HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 resume filesystems keyboard fsck)
```

# Configure bootloader

See /boot for the exact filenames:

```shell-session
ls -l /boot
total 114968
drwxr-xr-x 5 root root     4096 Jan  1  2023 EFI
-rwxr-xr-x 1 root root 81461510 Oct 14 13:35 initramfs-linux-fallback.img
-rwxr-xr-x 1 root root 23459205 Oct 14 13:35 initramfs-linux.img
drwxr-xr-x 3 root root     4096 Nov  1 18:04 loader
-rwxr-xr-x 1 root root 12787840 Oct 14 13:35 vmlinuz-linux
```

## LVM only

**NOTE:** For ARM use *linux /Image* instead of *linux /vmlinuz-linux*

* Create /boot/loader/entries/arch.conf, check the options part behind `/dev/mapper` on your system

```text
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=/dev/mapper/vg1-root rw
```

## LVM and LUKS

* Create /boot/loader/entries/arch.conf, cryptdevice= will point to our second partition /dev/sda2 where we created the LVM
* Find out the device's uuid end echo it to arch.conf to help you edit it

```shell-session
blkid | grep /dev/sda2
/dev/sda2: UUID="ca31d595-1e1c-4c3a-a961-d66e5102298b" TYPE="crypto_LUKS" PARTLABEL="Linux filesystem" PARTUUID="2b32cb0a-2f15-4947-92cc-5fc76bef2fec"
```

```shell-session
blkid | grep /dev/sda2 >> /boot/loader/entries/arch.conf
nvim /boot/loader/entries/arch.conf
```

* Fill the file with:

**NOTE:** For ARM use *linux /Image* instead of *linux /vmlinuz-linux*

```text
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options cryptdevice=UUID=ca31d595-1e1c-4c3a-a961-d66e5102298b:lvm:allow-discards resume=/dev/mapper/vg0-swap home=/dev/mapper/vg0-home root=/dev/mapper/vg0-root rw quiet
```

## Without LUKS and LVM

```shell-session
blkid | grep /dev/sda2 >> /boot/loader/entries/arch.conf
nvim /boot/loader/entries/arch.conf
```

* Fill the file with:

**NOTE:** For ARM use *linux /Image* instead of *linux /vmlinuz-linux*

```text
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=UUID=ca31d595-1e1c-4c3a-a961-d66e5102298b resume=UUID=ds22e484-2d9d-2d3b-a961-b66f1029813c rw quiet
```

# Update /boot/loader/loader.conf

```text
timeout 3
default arch
```

# Create initial ramdisk

```shell-session
mkinitcpio -P
```

# Finish installation and boot into new system

* **NOTE 1:** You will boot into an enviorement with no GUI and no network. See the *Connect to WiFi* steps to connect to a WiFi network. For a GUI see [Gnome.md](Gnome.md) for instructions.
* **NOTE 2:** For tips and tricks see [this page](Tips_n_tricks.md) for more info.

```shell-session
exit
umount -R /mnt
reboot
```

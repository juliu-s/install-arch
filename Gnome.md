# Gnome

# Installing packages

* Use [this link](https://archlinux.org/groups/x86_64/gnome/) for help selecting the components you want for the Gnome package group
* Some extra packages for a better user experience

```shell-session
pacman -S gnome networkmanager seahorse gnome-terminal firefox
```

# Enable GDM service

```shell-session
systemctl enable gdm.service
```

# Enable NetworkManager service

```shell-session
systemctl enable NetworkManager
```

# Reboot

```shell-session
reboot
```

# Gnome integrations

* See the [tips and tricks page](Tips_n_tricks.md)
